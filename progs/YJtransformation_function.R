# for nifti image IO
# for yeo.johnson transformation
# for pblapply
# to get number of cores
packages = c('RNifti', 'VGAM', 'pbapply', 'parallel')
ptoinstall = which(!packages %in% row.names(installed.packages()))
if(length(ptoinstall>0)){
    cat('installing required packages\n')
    install.packages(packages[which(!packages %in% row.names(installed.packages()))], repos="http://cran.cnr.berkeley.edu/")
}

for(p in packages){
   require(p, character.only=TRUE)
}

# lambda out is a nifti image with the transformation parameter
# fourd out is a 4d nifti image of the transformed data
YJtransform = function(fourd=NULL, mask=NULL, X=NULL, lambdaout=NULL, fourdout=NULL, ncores=round(detectCores()*3/4), eps=0.001){
  # compute residual forming matrix
  n = nrow(X)
  xqr = qr(X)
  
  # profile likelihood for YJ transformation. Uses xqr in function definition
  loglik = function(lambda, y){
    n/2*log(sum(qr.resid(xqr, yeo.johnson(y, lambda, epsilon = eps))^2) ) + (1-lambda) * (sign(y) %*% log1p(abs(y)))
  }

  if(is.null(mask)){
    cat('No mask provided. No output images will be saved.\n')
  }else if(is.character(mask) & file.exists(mask) ){
    mask = readNifti(mask)
  }
  
  if(is.character(fourd)){
    if(file.exists(fourd)){
      cat("loading 4d image\n")
      inputmat=FALSE
      fourd = readNifti(fourd)
      fourd = fourd[ mask!=0]
      fourd = matrix(fourd, sum(mask!=0), n)
      fourd = lapply(1:nrow(fourd), function(x) fourd[x,])
    } else {
      stop(paste("Cannot find image:", fourd))
    } 
  }else if(is.matrix(fourd)){
    inputmat=TRUE
    fourd = lapply(1:ncol(fourd), function(x) fourd[,x])
    cat('"fourd" argument is a matrix.\n')
  }
  
  cat("estimating parameter map\n")
  #esttime = system.time(lambda <- mclapply(fourd, function(y) optimize(f, interval=c(0,2), y=y)$minimum, mc.cores=round(detectCores()*3/4) ))
  esttime = system.time(lambda <- pblapply(fourd, function(y) optimize(loglik, interval=c(-2,2), y=y)$minimum, cl=ncores ) )
  lambda = unlist(lambda)
  
  ### WRITE OUTPUT ###
  if(!is.null(lambdaout) & !is.null(mask)){
    cat('writing parameter image\n', lambdaout, '\n')
    imgout = array(0, dim=dim(mask))
    imgout[ mask!=0 ] = lambda
    writeNifti(imgout, file=lambdaout, template=mask)
  }

  if(inputmat | (!is.null(fourdout) & !is.null(mask))){
    fourd = lapply(1:length(fourd), function(x) c( lambda[x], fourd[[x]]) )
    fourd = mclapply(fourd, function(x) yeo.johnson(x[-1], lambda=x[1]), mc.cores=ncores  )
    fourd = do.call(rbind, fourd)
  }
  
  if(!is.null(fourdout) & !is.null(mask)){
    cat('writing transformed 4d data\n', fourdout, '\n')
    imgout = array(0, dim=c(dim(mask), n) )
    imgout[ mask!=0 ] = c(fourd)
    rm(fourd)
    writeNifti(imgout, file=fourdout, template=mask)
  }

  if(inputmat==TRUE){
   return(t(fourd) )
  }
}
