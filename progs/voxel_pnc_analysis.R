rm(list=ls())
#library(voxel)
library(oro.nifti)
library(parallel)
library(Rcpp)
library(car)
#library(multtest)
source('/project/taki2/pnc/param-boot/progs/YJtransformation_function.R')
# the VGAM package loaded in the script above masks "s" from mgcv
library(mgcv)

# smoothing parameter
sm=0
# for cluster based thresholding
thresh=0.01
nnullsim=10000
nsim=10000
k=5

# load functions for this paper
progsdir='/project/taki2/pnc/param-boot/progs'
sdir = '/project/taki2/pnc/subject_lists'
thisdir = getwd()
source(file.path(progsdir, 'functions.R'))
sourceCpp(file.path(progsdir, 'pb.cpp'))
source(file.path(progsdir, 'MTP.R'))
source(file.path(progsdir, 'lmX.R') )
system("module load c3d")


home='/project/taki2/pnc/param-boot'
imagedir=file.path(home, '../n1601_dataFreeze2016/neuroimaging/asl/voxelwiseMaps_cbf')
maskfile = file.path(home, '../n1601_dataFreeze2016/neuroimaging/asl/n1601_PcaslCoverageMask_grey_thr01_2mm.nii.gz')
outdir = "/project/taki2/pnc/param-boot/pnc_results"
outdir = file.path(outdir, paste('sm', sm, '_k', k, sep=''))
dir.create(outdir, showWarnings=FALSE)

# load cleaned data (after exclusions for LTN and imaging QA).
# See /project/taki2/pnc/subject_lists/param-boot_sample.R for exclusions.
pnc = readRDS(file.path(sdir, 'n972_demog_jflPcasl_20161101.rds') )
pnc$sex = ordered(pnc$sex)
# original file location
pnc$file = sapply(paste(pnc$scanid, '_asl_quant_ssT1Std.nii.gz', sep=''), function(x) list.files(imagedir, pattern=x, full.names=TRUE))
# smooth file location CBF IMAGES ARE PRESMOOTHED
if(sm!=0){
	pnc$file = gsub('voxelwiseMaps_cbf', paste('sm', sm, 'FWHM', sep=''), pnc$file)
}
set.seed(777)

# MODEL FORMULAS
form = paste(" ~ race + sex + pcaslRelMeanRMSMotion + pcaslRpsMapCorrectionNotApplied + s(age, fx=TRUE, k=", k, ") + s(age, fx=TRUE, by=sex, k=", k, ")" )
formred = paste(" ~ race + sex + pcaslRelMeanRMSMotion + pcaslRpsMapCorrectionNotApplied + s(age, fx=TRUE, k=", k, ")")
lmfull = paste("pcasl_jlf_cbf_R_Accumbens", form)
lmred = paste("pcasl_jlf_cbf_R_Accumbens", formred)
lmfull = model.matrix(gam(as.formula(lmfull), data=pnc))
lmred = model.matrix(gam(as.formula(lmred), data=pnc) )
#mods = anovagamVoxel(pnc$file, maskfile, fourdOut=outfile, formula=form, subjData=pnc)
#anova(mod)$s.table[2,2:4]

### RUN RANDOMISE PERMUTATION TESTS ###
# commented out to rerun reduced model
if( ! file.exists(file.path(outdir, "randomise_vox_corrp_fstat1.nii.gz") ) ){
	randstart = proc.time()[3]
	randomisecmds = randomise(filelist=pnc$file, dat=pnc, maskfile=maskfile, X=lmfull, Xred=lmred, outdir=outdir, nsim=nsim, run=TRUE)
	randend = proc.time()[3]
	randtime = randend - randstart
	system(paste("c3d ", file.path(outdir, "randomise_vox_corrp_fstat1.nii.gz"), "-shift -1 -scale -1 -log10 -scale -1 -o", file.path(outdir, "voxel_randomise_log10.nii.gz")) )
}

# Estimate parameters for YJ transformation.
# By default runs in parallel using 3/4 of total
# number of cores on host machine.
fourd = file.path(outdir, 'merged.nii.gz')
bcparamimg = file.path(outdir, 'YJtransform_lambda.nii.gz')
fourdout = file.path(outdir, 'merged_YJtransformed.nii.gz')
if(!file.exists(fourdout)){
	if(!file.exists(bcparamimg)){
		YJtransform(fourd=fourd, mask=maskfile, X=lmfull, lambdaout=bcparamimg, fourdout=fourdout)
	}
}

# RUN RANDOMISE ON THE TRANSFORMED DATA
YJrandomisedir = file.path(outdir, 'YJrandomise')
if( ! file.exists(file.path(YJrandomisedir, "randomise_vox_corrp_fstat1.nii.gz") ) ){
	dir.create(YJrandomisedir, showWarnings=FALSE)
	yjrandstart = proc.time()[3]
	YJrandomisecmds = randomise(filelist=fourdout, dat=pnc, maskfile=maskfile, X=lmfull, Xred=lmred, outdir=YJrandomisedir, nsim=nsim, run=TRUE)
	yjrandend = proc.time()[3]
	yjrandtime = yjrandend - yjrandstart
	system(paste("c3d ", file.path(YJrandomisedir, "randomise_vox_corrp_fstat1.nii.gz"), "-shift -1 -scale -1 -log10 -scale -1 -o", file.path(outdir, "YJtransformed_voxel_randomise_log10.nii.gz")) )
}

# the randomise command makes all design files except the grp file
grpfile = file.path(outdir, "design.grp")
cat('/NumWaves\t1\n/NumPoints\t', nrow(pnc), '\n/Matrix\n', file=grpfile)
write.table(rep(1,nrow(pnc)), append=TRUE, file=grpfile, row.names=FALSE, col.names=FALSE)

# run reduced model to get residuals and compare
## DESIGN AND CONTRASTS ##
# design file
matfile = file.path(outdir, 'design_red.mat')
cat('/NumWaves\t', ncol(lmred), '\n/NumPoints\t', nrow(lmred), '\n/PPheights\t', paste(apply(lmred, 2, function(x) abs(diff(range(x))) ), collapse='\t'), '\n\n/Matrix\n', sep='', file=matfile)
write.table(lmred, append=TRUE, file=matfile, row.names=FALSE, col.names=FALSE)

# contrast file
confile1 = file.path(outdir, 'design_red.con') # for f-test
cons = matrix(0, nrow=1, ncol=ncol(lmred))
cons[ 1, 1] = 1
cat('/ContrastName1\t temp\n/ContrastName2\t\n/NumWaves\t', ncol(lmred), '\n/NumPoints\t', nrow(cons), '\n/PPheights\t', paste(rep(1,ncol(cons)), collapse='\t'), '\n/RequiredEffect\t1\t1\n\n/Matrix\n', sep='', file=confile1)
write.table(cons, append=TRUE, file=confile1, row.names=FALSE, col.names=FALSE)


# run unpenalized spline model using flameo
setwd(outdir)
lmcmds = paste("flameo --cope=", fourd, " --mask=", maskfile, " --dm=", file.path(outdir, 'design.mat'), " --tc=", file.path(outdir, "design.con"), " --cs=", file.path(outdir, "design.grp"), " --fc=", file.path(outdir, "design.fts"), " --runmode=ols", sep="")
if(!file.exists('untransformed')){
	cat(lmcmds)
	system(lmcmds)
	system('mv logdir untransformed')
}
setwd(thisdir)

# RUNS flameo in the transformed image
pbstart = proc.time()[3]
setwd(outdir)
lmcmds = paste("flameo --cope=", fourdout, " --mask=", maskfile, " --dm=", file.path(outdir, 'design.mat'), " --tc=", file.path(outdir, "design.con"), " --cs=", file.path(outdir, "design.grp"), " --fc=", file.path(outdir, "design.fts"), " --runmode=ols", sep="")
if(!file.exists('YJtransformed')){
	cat(lmcmds)
	system(lmcmds)
	system('mv logdir YJtransformed')
}
setwd(thisdir)


### PERFORM PARAMETRIC BOOTSTRAP ###
# USES YJ-transformed images
	modeldir='YJtransformed'
	resids = readNIfTI(file.path(outdir, modeldir, 'res4d.nii.gz'))
	resids = apply(resids@.Data, 4, c)
	mask = readNIfTI(maskfile)
	resids = t(resids[c(mask@.Data)==1,])
	resids = sweep(resids, 2, sqrt(colSums(resids^2)), '/')
	pvals = readNIfTI(file.path(outdir, modeldir, 'zfstat1.nii.gz'))@.Data[ mask@.Data==1]
	pvals = pnorm(pvals, lower.tail=FALSE)
	out = PBadjust(p.values = pvals, residuals = resids, df = ncol(lmfull)-ncol(lmred), rdf=nrow(pnc)-ncol(lmfull), nsim = nnullsim, step = 'ss', compare = TRUE)
	pbend = proc.time()[3]
	pbtime = pbend - pbstart


	# write out pvalue images
	out.pval = file.path(outdir, 'voxel_uncorrected_log10p.nii.gz')
	imgout = array(0, dim=dim(mask))
	imgout[ mask@.Data!=0 ] = -log10(c(out$pvalues$uncorrected[,1]))
	imgout = nifti(imgout, datatype=16)
	imgout = copyhdr(mask, imgout)
	writeNIfTI(imgout, rmniigz(out.pval))

	out.pval = file.path(outdir, 'voxel_Bonferroni_log10p.nii.gz')
	imgout = array(0, dim=dim(mask))
	imgout[ mask@.Data!=0 ] = -log10(c(out$pvalues$Bonferroni[,1]))
	imgout = nifti(imgout, datatype=16)
	imgout = copyhdr(mask, imgout)
	writeNIfTI(imgout, rmniigz(out.pval))

	out.pval = file.path(outdir, 'voxel_Holm_log10p.nii.gz')
	imgout = array(0, dim=dim(mask))
	imgout[ mask@.Data!=0 ] = -log10(c(out$pvalues$Holm[,1]))
	imgout = nifti(imgout, datatype=16)
	imgout = copyhdr(mask, imgout)
	writeNIfTI(imgout, rmniigz(out.pval))

	out.pval = file.path(outdir, 'voxel_PB_log10p.nii.gz')
	imgout = array(0, dim=dim(mask))
	imgout[ mask@.Data!=0 ] = -log10(c(out$pvalues$PB[,1]))
	imgout = nifti(imgout, datatype=16)
	imgout = copyhdr(mask, imgout)
	writeNIfTI(imgout, rmniigz(out.pval))

#}

objects = c("lambdatime", "randtime", "pbtime", 'yjrandtime')
subobjects = objects[ which( sapply(objects, exists))]
resave(list = subobjects, file=file.path(outdir, 'pnc_execution_times.rdata'))
