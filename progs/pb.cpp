#include <RcppArmadillo.h>
//#include <armadillo>
#include <string.h>
#include <cmath>
#include <Rmath.h>
#include <algorithm>
#include <random>
#include <ctime>
// [[Rcpp::plugins(openmp)]]

// see http://stackoverflow.com/questions/29311481/sourcecpp-upcoming-iso-standard
// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::depends(RcppArmadillo)]]

using namespace arma;
using namespace Rcpp;

// returns cumulative maximums only in the intervals defined by maxes
vec cummaxk(vec x, vec maxes){
  int k = maxes.n_elem;
  vec out(k);
  out(0) = x(0);
  for(int i = 1; i < k; i++){
    // doesn't matter that there's overlap on the edges b/c we will take cummax
    out(i) = max( x.subvec(maxes(i-1)-1, maxes(i)-1) );
    out(i) = (out(i)<out(i-1))?out(i-1):out(i);
  }
  return out;
}

// returns cumulative maximums
vec cummax(vec x){
  vec out(x);
  out(0) = x(0);
  for(int i = 1; i < x.n_elem; i++){
    out(i) = (out(i)<out(i-1))?out(i-1):out(i);
  }
  return out;
}

vec randF( mat V, int df){
  mat X = randn<mat>(V.n_cols, df); // (n-p) x (n-p)
  vec num(V.n_rows);
  num = sum(square(V * X), 1);
  return(num);
}

// This function assumes that all columns of M are unit vectors
// It does not check that this is true.
// [[Rcpp::export]]
mat lowRankPB(mat M, int nsim, int df, int rdf, const char * step) {
  const char * ss="ss";
  // assumes columns of M are standardized
  mat U; vec s; mat V;
  svd_econ(U, s, V, M, 'r');
  // rank of covariance matrix
  int r = (rdf<s.n_elem)?rdf:s.n_elem;
  vec t2(r);
  t2 = s.subvec(0,r-1);
  Rcout << "Residual DOF " << rdf << '\n';
  Rcout << "Covariance rank " << r << '\n';
  if( V.n_cols >= M.n_rows){
    V.shed_cols(r, M.n_rows-1);
  }
  //Rcout << V.n_rows << '\t' << V.n_cols << '\t' << s.n_elem << '\n';
  V.each_row() %= t2.t();
  mat O;
  if( strcmp(step, ss)==0 ){
  O = mat(nsim, 1);
    // If df==0 then this will only return normal data in the first column
    // This is for F-tests:
    if(df!=0){
      for(int i = 0; i < nsim; i++) {
        //Rcout << U.n_cols << ',' << U.n_rows << '\n';
	//Rcout << i << '\t';
        vec U = randF( V, df);
        O(i) = max(U);
      }
    }
    // This is for T-tests:
    else {
      for(int i = 0; i < nsim; i++) {
        vec Y = randn(r);
        vec U = abs(V * Y);
        O(i) = max(U);
      }
    }
  }
  // This is for the step-down procedure
  else {
    O=mat(V.n_rows, nsim);
    if(df!=0){
      for(int i = 0; i < nsim; i++) {
        //Rcout << U.n_cols << ',' << U.n_rows << '\n';
        vec U = randF( V, df);
        O.col(i) = cummax(U);
      }
    }
    else {
      for(int i = 0; i < nsim; i++) {
        vec Y = randn(r);
        vec U = abs(V * Y);
        O.col(i) = cummax(U);
      }
    }
  }
  return O;
}
