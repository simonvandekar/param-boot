require(MASS)
#require(mvtnorm)

# modified holm procedure
# assumes p-values are in the same order as the covariance (or correlation) matrix
mh <- function(ps, eigmat, covhat, nsim=10e+05){
	# check Sigma is symmetric
# 	if(any(round(sigma[lower.tri(sigma)], 8) != round(t(sigma)[lower.tri(sigma)], 8) )){
# 		stop('covariance matrix is not symmetric!')
# 	}
# 	if(length(ps) != ncol(sigma)){
# 		stop('size covariance matrix does not match number of p-values!')
# 	}

	# reorder p-values to decreasing
	ords <- order(ps, decreasing=TRUE)
	eigmat <- eigmat[ ords, ] # shouldn't need to exchange the columns
	ps <- ps[ ords]
	#ms <- rmvnorm(nsim, sigma=sigma )
  # from MASS package
	ms <- eigmat %*% matrix(rnorm(nsim*p), nrow=p, ncol=nsim) 
	# mvrnorm(nsim, mu=rep(0, ncol(sigma)), Sigma=sigma) too slow

	p <- length(ps)
	# replace ms
	ms <- apply(abs(ms), 2, cummax)
	# this is the inverse CDF of the folded normal
	zs <- qnorm((2-ps)/2, mean=0, sd=sqrt(covhat) )
	p.mh <- rowMeans(ms>zs)
	# sort ascending and take cummax of corrected p-values
  # this was for holm's procedure
	p.mh <- cummax(p.mh[p:1])[p:1]
  # this is hochberg, which is appropriate here
  #p.mh <- cummin(p.mh)
	p.h <- p.adjust(ps, method='holm')
	# if modified procedure has larger p-value it is due to rounding error, so replace with holm p-value
	p.mh <- ifelse(p.mh > p.h, p.h, p.mh)
	# if p-value is too small to get with simulations replace with holm p-value
	p.mh <- ifelse(p.mh==0, p.h, p.mh)
	return(p.mh[order(ords)])
}



# modified holm procedure
# assumes p-values are in the same order as the covariance (or correlation) matrix
# for rank deficient covariance matrix
mhrd <- function(ps, M, covhat, nsim=10e+05){
  # reorder p-values to decreasing
  ords <- order(ps, decreasing=TRUE)
  M <- M[ , ords]
  n = nrow(M)
  m = ncol(M)
  ps <- ps[ ords]
  o <- min((n-2), m)
  svd.M = svd(M, nu=0, nv=o ) # (n-2) because means are estimated
  ms <- sweep(svd.M$v, 2, svd.M$d[1:o]/sqrt(n-2), FUN="*") %*% matrix(rnorm(nsim*o ), nrow=(o), ncol=nsim)
  #ms <- lowRankSim(M, nsim), replications = 20) this is very slightly faster
  
  p <- length(ps)
  # replace ms
  ms <- apply(abs(ms), 2, cummax)
  # this is the inverse CDF of the folded normal
  zs <- qnorm((2-ps)/2, mean=0, sd=sqrt(covhat) )
  p.mh <- rowMeans(ms>zs)
  # sort ascending and take cummax of corrected p-values
  # this was for holm's procedure
  p.mh <- cummax(p.mh[p:1])[p:1]
  # this is hochberg, which is appropriate here
  #p.mh <- cummin(p.mh)
  p.h <- p.adjust(ps, method='holm')
  # if modified procedure has larger p-value it is due to rounding error, so replace with holm p-value
  p.mh <- ifelse(p.mh > p.h, p.h, p.mh)
  # if p-value is too small to get with simulations replace with holm p-value
  p.mh <- ifelse(p.mh==0, p.h, p.mh)
  return(p.mh[order(ords)])
}


# permute = function(samp, nsim=nsim){
#   n = nrow(samp)
#   resamp = samp[sample(1:n),]
#   resampmu = resamp[1:(n/2),] - resamp[(n/2+1):n,]
#   covhat = (apply(resamp[1:(n/2),],2, var) + apply(resamp[(n/2+1):n,], 2, var))/(n/2)
#   muhat = abs(colMeans(resampmu))/sqrt(covhat)
# }

wy = function(samp, stats, pvals, nsim=500){
  ord = order(pvals, decreasing=TRUE)
  samp = samp[, ord]
  stats = stats[ord]
  #nulldist <- replicate(nsim, permute(samp)), # takes 6 times longer with nsim=500
  nulldist = wynull(samp, nsim) # this is a c++ script
  nulldist = apply(nulldist, 2, cummax)
  corpvals = cummax(rowMeans(nulldist>abs(stats))[length(pvals):1])[length(pvals):1]
  corpvals[order(ord)]
}
