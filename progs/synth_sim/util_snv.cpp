#include <RcppArmadillo.h>
//#include <armadillo>
#include <cmath>
#include <Rmath.h>
#include <algorithm>
#include <random>

// see http://stackoverflow.com/questions/29311481/sourcecpp-upcoming-iso-standard
// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::depends(RcppArmadillo)]]

using namespace arma;
using namespace Rcpp;
/*
// obs sorted smallest to largest
// [[Rcpp::export]]
mat mhbs(mat obs, mat M, int nsim) {
  int ncols = M.n_cols; // number of test statistics
  int nrows = M.n_rows; // number of subjects
  int ncolsobs = obs.n_cols; // either 1 or 2. Standardized and/or unstandardized statistics.
  mat U;
  vec s;
  mat V;
  svd_econ(V, s, U, M, "left");
  mat D = M.t() * V;
  
 // generate random matrix
  mat Z = randn(nrows, nsim);
  mat Y = D * Z;
  mat res(ncols, ncolsobs, fill::zeros);
  
  for(int j = 0; j < nsim; j++) {
    for(int i = 0; i < ncolsobs; i++) {
      res.col(i) = CUMMAX(Y.col(j)); //(CUMMAX(Y.col(j)) > obs.col(i));
    }
  }
  return(res / nsim); // p-values
}
*/

//auto engine = std::default_random_engine{};

// [[Rcpp::export]]
mat lowRankSim(mat M, int nsim) {
  int n = M.n_rows;
  mat Y = randn(n, nsim);
  mat U; vec s; mat V;
  svd_econ(U, s, V, M, 'r');
  return  V * Y;
}

// [[Rcpp::export]]
mat eigArma(mat sigma){
  vec eigval;
  mat eigvec;
  eig_sym( eigval, eigvec, sigma);
  return eigvec;
}

// permute and compute sample statistics
mat permutecompute(mat samp){
  mat resamp = shuffle(samp, 0);
  
  int n = resamp.n_rows;
  mat resamp1 = resamp.rows(0,(n/2-1));
  mat resamp2 = resamp.rows((n/2), n-1);
  mat mumat = resamp1 - resamp2;
  mat covhat = (var(resamp1, 0, 0) + var(resamp2, 0, 0))/n*2;
  //covhat = (apply(resamp[1:(n/2),],2, var) + apply(resamp[(n/2+1):n,], 2, var))/(n/2)
  mat mus = mean(mumat, 0);
  mat muhat = ( mus % sign(mus))/sqrt(covhat);
  return muhat;
}

// [[Rcpp::export]]
mat wynull(mat samp, int nsim){
  int p = samp.n_cols;
  mat out(p, nsim);
  for(int i = 0; i < nsim; i++) {
    out.col(i) = permutecompute(samp).t();
  }
  return out;
}
