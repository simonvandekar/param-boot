# functions specifically for dealing with the simulated ROI data

roiPBsim = function(dat=NULL, lmfull=NULL, rois=NULL, tests=c('perm_group1', 'perm_group'), nsim=5000, ncores=6){
	images = as.matrix(dat[,rois])

	# Perform YJ-transformation. ADDED 20170718
	images = YJtransform(fourd=images, X=model.matrix(lmfull, data=dat), ncores=ncores*2)

	# computes p-values for all t-test and p-values for the f-test of 'perm_group'
	pvals = pcompute(dat=dat, lmfull=lmfull, images=images, tests=tests)
	rm(images)
	resids = pvals[['resids']]

	# single-step and step-down p-values
	sst.tests = PBadjust(pvals[['tp.values']], residuals=resids, df=0, rdf=pvals$rdf, step='ss', compare=TRUE)
	sdt.tests = PBadjust(pvals[['tp.values']][, 'perm_group1'], residuals=resids, df=0, rdf=pvals$rdf, step='sd', compare=FALSE)
	# this is not design for vector p2s
	if('fp.values' %in% names(pvals) ){
		# only does first F-test
		ssf.tests = PBadjust(pvals[['fp.values']][,1,drop=FALSE], residuals=resids, df=pvals[['dfs']][1], rdf=pvals$rdf, step='ss', compare=TRUE)
		sdf.tests = PBadjust(pvals[['fp.values']][,1,drop=FALSE], residuals=resids, df=pvals[['dfs']][1], rdf=pvals$rdf, step='sd', compare=FALSE)
		names(ssf.tests) = paste('SSF', names(ssf.tests), sep='_')
		names(sdf.tests) = paste('SDF', names(sdf.tests), sep='_')
	}
	names(sst.tests) = paste('SST', names(sst.tests), sep='_')
	names(sdt.tests) = paste('SDT', names(sdt.tests), sep='_')

	#### OUTPUT ####
	outdf = c(sst.tests, sdt.tests)
	if('fp.values' %in% names(pvals) ){
		outdf = c(outdf, ssf.tests, sdf.tests )
	}
	return(outdf)
}

roiNPBsim = function(dat=NULL, lmfull=NULL, rois=NULL, tests=c('perm_group1', 'perm_group'), nsim=5000, ncores=6){
	if(any(is.null(c(lmfull, dat)))){
		stop('Specify a model and data frame.')
	}
	images = as.matrix(dat[,rois])
	X = model.matrix(lmfull, data=dat)

	# LM version not supported for permutation distribution. Will just use bootstrap version for paper.
	# The bootstrap version takes a long time for whatever reason
	bootTsdout = MTP(X=t(images), Z=X, Z.test=grep(tests[1], colnames(X), value=TRUE), Z.incl=grep(tests[1], colnames(X), invert=TRUE, value=TRUE), test='lm.XvsZ', nulldist='boot.cs', B=nullnsim, method=c('sd.minP'), keep.nulldist=FALSE, get.cutoff=TRUE)
	maxcutoff = bootTsdout@cutoff[which.max(bootTsdout@statistic)]
	t.tests = data.frame(ssboot=abs(bootTsdout@statistic) > maxcutoff, sdboot=bootTsdout@adjp)

	# perform F-statistic tests
	# produces known warnings
	bootFsdout = MTPmod(X=t(images), Z=X, Z.test=grep(tests[2], colnames(X), value=TRUE), Z.incl=grep(tests[2], colnames(X), invert=TRUE, value=TRUE), test='lm.XvsZ', nulldist='boot.cs', B=nullnsim, method=c('sd.maxT'), keep.nulldist=FALSE, get.cutoff=TRUE, alternative='greater')
	#bootFsdout = MTP(X=RF, Y=as.numeric(dat[,tests[2]])-1, test='f', nulldist='boot.cs', B=5000, method=c('sd.maxT'), keep.nulldist=FALSE, get.cutoff=TRUE)
	maxcutoff = bootFsdout@cutoff[which.max(bootFsdout@statistic)]
	f.tests = data.frame(ssboot=abs(bootFsdout@statistic) > maxcutoff, sdboot=bootFsdout@adjp)

	# return step-down permutation and bootstrap p-values. Single-step just gives which tests are rejected.
	out = list('t.tests'=t.tests, 'f.tests'=f.tests)
	return(out)
}

roiRandomisesim = function(dat=NULL, lmfull=NULL, lmred=NULL, rois=NULL, simdir=NULL, nsim=5000, ncores=6){
	if(any(is.null(c(lmfull, dat)))){
		stop('Specify a model and data frame.')
	}
	images = as.matrix(dat[,rois])
	X = model.matrix(lmfull, data=dat)
	Xred = model.matrix(lmred, data=dat)
	niftifile = file.path(simdir, 'roi_merged.nii.gz')
	# I checked that this returns a nifti image that is Vx1x1xn, where each "volume" is a subject
	roi2nifti( array(t(dat[, rois]), dim=c(length(rois), 1, 1, nrow(dat))), file=niftifile)
	# DO F-TEST
	randomisecmds = randomise(filelist=niftifile, dat=dat, X=X, Xred=Xred, outdir=simdir, nsim=nsim, run=TRUE)
	# randomise gives 1 minus p-values
	ftestpermout = 1-nifti2roi(file.path(simdir, 'randomise_vox_corrp_fstat1.nii.gz'), rois)
	ftestuncorrpermout = 1-nifti2roi(file.path(simdir, 'randomise_vox_p_fstat1.nii.gz'), rois)

	# Do T-TEST (with an F-statistic)
	Xred = X[,grep('perm_group1', colnames(X), invert=TRUE)]
	randomisecmds = randomise(filelist=niftifile, dat=dat, X=X, Xred=Xred, outdir=simdir, nsim=nsim, run=TRUE)
	# randomise gives 1 minus p-values
	ttestpermout = 1-nifti2roi(file.path(simdir, 'randomise_vox_corrp_fstat1.nii.gz'), rois)
	ttestuncorrpermout = 1-nifti2roi(file.path(simdir, 'randomise_vox_p_fstat1.nii.gz'), rois)

	out = list(f.tests = cbind( UncorrPermutation=ftestuncorrpermout, Permutation=ftestpermout), t.tests = cbind( UncorrPermutation=ttestuncorrpermout, Permutation=ttestpermout) )
	rownames(out[['f.tests']]) = rois
	rownames(out[['t.tests']]) = rois
	return(c(out, randomisecmds)) # returns only the t-test randomise commands. F-test files are overwritten
}

